const express = require('express');
const multer = require('multer');
const fs = require('fs');
const cors = require('cors');
const winston = require('winston');
const dotenv = require('dotenv').config();
const { v4: uuidv4 } = require('uuid');

const app = express();
app.use(cors());
const port = process.env.PORT || 5555; // Usa el puerto del archivo .env o el predeterminado (3000)
const host = process.env.HOST || '127.0.0.1';

const createDirectories = () => {
  const directories = ['./logs'];

  directories.forEach(directory => {
    if (!fs.existsSync(directory)) {
      fs.mkdirSync(directory);
    }
  });
};

createDirectories();

// Configuración de Winston para el sistema de logging
const logger = winston.createLogger({
  level: process.env.LOG_LEVEL || 'info',
  format: winston.format.combine(
    winston.format.timestamp(),
    winston.format.printf(info => `${info.timestamp} - ${info.level}: ${info.message}`)
  ),
  transports: [
    new winston.transports.Console(),
    new winston.transports.File({ filename: './logs/video_receiver.log' })
  ]
});

// Imports the Google Cloud client library
const {Storage} = require('@google-cloud/storage');

// Crear una instancia del cliente de Google Cloud Storage
const storageGcp = new Storage({
  projectId: 'sdpp2-405215',
  // keyFilename: 'sdpp2-405215-a21bf250e80c.json',
  keyFilename: "/etc/secret-volume/sdpp2-405215-a21bf250e80c.json"
});
const bucketName = 'videos-to-process-sobel';

// Configurar Multer para manejar archivos en memoria
const storage = multer.memoryStorage(); // Almacena el archivo en memoria

const upload = multer({ storage });

// Ruta POST para recibir el video
app.post('/videoreceiver', upload.single('video'), async (req, res) => {
  try {
    logger.info(`Se recibió una petición de ${req.ip}`);

    const videoId = uuidv4().replace(/-/g, ''); // Genera un ID único
    const fileName = req.file.originalname; // Obtiene el nombre original del archivo
    // Configura el nombre de destino en GCS
    const destFileName = `${videoId}-${fileName}`;    

    async function uploadFromMemory() {
      await storageGcp.bucket(bucketName).file(destFileName).save(req.file.buffer);
    
      logger.info(
        `${destFileName} uploaded to ${bucketName}.`
      );
    }
    logger.info(`Video recibido: ${fileName}`);
    uploadFromMemory().catch(console.error);
    logger.info(`Video cargado en GCS: ${destFileName}`);
    return res.status(200).json({'videoId':destFileName});
  } catch (error) {
    logger.error(`Error al procesar el video: ${error.message}`);
    return res.status(500).json({ error: 'Error al procesar el video' });
  }
});

app.listen(port, () => {
  logger.info(`Servidor escuchando en puerto ${port}`);
  logger.info(`listando objetos en el bucket...`);
  async function listFiles() {
    // Lists files in the bucket
    const [files] = await storageGcp.bucket(bucketName).getFiles();

    logger.info('Files:');
    files.forEach(file => {
      logger.info(file.name);
    });
  }

  listFiles().catch(console.error);
});
