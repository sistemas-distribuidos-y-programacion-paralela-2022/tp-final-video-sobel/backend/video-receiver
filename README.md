# video-receiver
## Guia de ejecucion del Servicio
>>> 1. Tener instalado node.js previamente.
>>> 2. Ubicarse en la carpeta raíz del proyecto llamada main-form
>>> 3. Instalar las dependencias:  ```npm install``` 
>>> 4. Levantar el servicio:  ```node VideoReceiver.js``` 

Luego por defecto el servicio estara escuchando en ```localhost:5555```.

## Despliegue en Docker
Se adjunto al proyecto un archivo Dockerfile. En el se encuentra específicado que version de Node a utilizar y además se automatiza la instalación de las dependencias y la ejecución del servidor.
Los pasos a realizar para desplegar son:
>>> 1. Ubicarse en el directorio del dockerfile y ejecutar : ```docker build -t video_receiver .``` 
>>> 2. Ejecutar el contenedor: ```docker run -p 5555:5555 video_receiver```

Aclaración:"video_receiver" puede ser el nombre que elijas.
